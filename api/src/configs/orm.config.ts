import { SequelizeModuleOptions } from '@nestjs/sequelize';
import { ConfigService } from '@nestjs/config';
import { User } from '../users/users.model';
import { Role } from '../roles/roles.model';
import { UserRole } from '../roles/users-roles.model';

const ormConfig = (configService: ConfigService) =>
  ({
    host: configService.get('POSTGRES_HOST'),
    port: configService.get('POSTGRES_PORT'),
    username: configService.get('POSTGRES_USER'),
    password: configService.get('POSTGRES_PASSWORD'),
    database: configService.get('POSTGRES_DATABASE'),
  } as SequelizeModuleOptions);

export const getPostgresConfig = async (
  configService: ConfigService,
): Promise<SequelizeModuleOptions> => {
  return {
    dialect: 'postgres',
    autoLoadModels: true,
    models: [User, Role, UserRole],
    ...ormConfig(configService),
  };
};
