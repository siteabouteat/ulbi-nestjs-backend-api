import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const PORT = process.env.PORT || 3000;
const MODE = process.env.NODE_ENV;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');

  const config = new DocumentBuilder()
    .setTitle('Nestjs swagger docs')
    .setDescription('Документация REST API')
    .setVersion('0.0.1')
    .addTag('hello world swagger')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('/api/docs', app, document);

  await app.listen(PORT);
}

bootstrap().then((_) =>
  console.log(
    `Server started success at http://localhost:${PORT} : MODE is ${MODE}`,
  ),
);
