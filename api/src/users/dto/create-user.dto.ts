import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({
    example: 'mail@mail.ru',
    description: 'Электронная почта',
    required: true,
    nullable: false,
  })
  readonly email: string;

  @ApiProperty({
    example: 'qwerty123',
    description: 'Пароль',
    required: true,
    nullable: false,
  })
  readonly password: string;
}
