import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from './users.model';
import { Role } from '../roles/roles.model';
import { UserRole } from '../roles/users-roles.model';

@Module({
  imports: [SequelizeModule.forFeature([User, Role, UserRole])],
  providers: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {}
