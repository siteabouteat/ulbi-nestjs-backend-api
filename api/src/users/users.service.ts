import { Injectable } from '@nestjs/common';
import { User } from './users.model';
import { InjectModel } from '@nestjs/sequelize';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User) private readonly userModel: typeof User) {}

  async create(dto: CreateUserDto): Promise<User> {
    return await this.userModel.create(dto);
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.findAll();
  }
}
