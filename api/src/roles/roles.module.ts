import { Module } from '@nestjs/common';
import { RolesController } from './roles.controller';
import { RolesService } from './roles.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { Role } from './roles.model';
import { User } from '../users/users.model';
import { UserRole } from './users-roles.model';

@Module({
  imports: [SequelizeModule.forFeature([Role, User, UserRole])],
  controllers: [RolesController],
  providers: [RolesService],
})
export class RolesModule {}
